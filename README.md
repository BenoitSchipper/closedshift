# Introduction

ClosedShift is focused around Openshift. I used these scripts to scale down my application pool on Openshift accross multiple namespaces.
I was asked to do this due to the fact that Openshift was being turned off temporarily due to data center Maintenance. Scaling the application pool down beforehand and up afterwards manually would prevent several hundreds of pods to be started at the same time. It allowed for manual control over over the environment and application pool.

## Requirements

- You will need to have BASH/GITBASH available to you.
- You will need to have the OC CLI available to you. [Link to Documentation](https://docs.openshift.com/container-platform/4.7/cli_reference/openshift_cli/getting-started-cli.html).

## Lessons Learned:

Here are some of the lessons I learned whilst making the scripts and preparing for data center maintenance:

- If you use HPA (Horizontal Pod Autoscalers), you will only need to remove them when using Openshift 3.
- Openshift 4 has a build in work around that allows for scaling down applications to 0, ignoring any HPA setup for that application/pod.
- For more information on HPA on Openshift 4 or latest K8s versions, [Click Here](https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale/#implicit-maintenance-mode-deactivation).
- Depending on how many pods you run per Namespace, scaling up Development, Test and Acceptance at the same time might cause some problems in Openshift 3. I suggest to scale them up per namespace individually instead.

---

## The Steps and Scripts I used to get through data center Maintenance without any problems.

Hereby sharing the steps and BASH scripts I used before and after the datacenter maintenance took place.
Please check the comments of each of the scripts before using them, some failsaves have been put in place to try and prevent accidental deletion.

**Important!**

- If you are using HPA on your deployments within Openshift 3, follow the below steps. 
- If you are using HPA on your deployments within Openshift 4, you can skip step 1, 2 and 6. If you scale down to 0 within Openshift 4, it ignores HPA. [More Information](https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale/#implicit-maintenance-mode-deactivation).

## Step 1 - Collect and store all HPA configuration in a hpalist.yaml file by Namespace && clean them up.

```
./gather_hpa.sh

```

## Step 2 - Then remove all HPA configuration in all our Namespaces. 

```
./delete_hpa.sh

```

## Step 3 - Scale all applications and other Pods to 0 && verify using metrics/logs that scaling to 0 has been successfully completed. 

```
./scale_down_dc.sh
./scale_down_sts.sh

```

## Step 4 - Wait until maintenance is completed.

```
apiVersion: coffeemachine/v1
kind: Coffee
metadata:
  name: coffee
spec:
  replicas: 1
  type: mug
  strength: strong

```

## Step 5 - Scale all applications and other Pods to 1 && verify using metrics/logs that scaling to 1 (or more) has been successfully completed. 

```
./scale_up_dc.sh
./scale_up_sts.sh

```

## Step 6 - Using the hpalist.yaml by namespace, restore all HPA configuration && verify using metrics/logs that scaling to 1 (or more) has been successfully completed.

```
./apply_hpa.sh

```

## Step 7 - Using metrics/logs to verify that the scaling and reinstatement of the HPA configuration was successfully performed.

- Grafana/ELK Stack Logging
- or what you happen to use =)

## Step 8 - Using automated regression, run tests on the Development, Test & Acceptance environments and smoketest or ask an end user to test Production. 

- This is a suggestion. However....
- Make sure you test the complete environment, preferably from top to bottom.

## Step 9 - Celebrate

![Celebrate](https://media.giphy.com/media/MhHXeM4SpKrpC/giphy.gif "Celebrate")


---

# Examples

Here are some examples in regards to namespace selection.
For example output in regards to the Horizontal Pod Autoscaler yaml files, see the "hpa-yaml-examples" folder.

**Example Output 'How to exclude Production'**
```
Command:
for NS in $(oc get projects | grep -e example-namespace1 -e example-namespace2 | grep -v -e production | awk '{print $1}')

Output:
example-namespace1-acceptance
example-namespace1-development
example-namespace1-test
example-namespace2-acceptance
example-namespace2-development
example-namespace2-test

```

**Example Output 'How to Exclude everything but production'**
```
Command:
for NS in $(oc get projects | grep -e example-namespace1 -e example-namespace2 | grep -e production | awk '{print $1}')

Output:
example-namespace1-production
example-namespace2-production

```
---

# License

MIT

## Made by

https://gitlab.com/BenoitSchipper
