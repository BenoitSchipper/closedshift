#!/bin/bash

#Scales down all deployments to 0.
#This is currently setup to IGNORE production, remove the second grep to include production. Or remove the -v to specifically select production instead.
for NS in $(oc get projects | grep -e example-namespace-range1 -e example-namespace-range2 | grep -v -e production | awk '{print $1}')
do
    oc scale dc --all --replicas=0 -n $NS && echo "Succesfully scaled deploymentConfigs to 0 within $NS"
done