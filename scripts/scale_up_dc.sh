#!/bin/bash

#Scales up all deployments to 1.
#This is currently setup to IGNORE production, remove the second grep to include production. Or remove the -v to specifically select production instead.
for NS in $(oc get projects | grep -e example-namespace-range1 -e example-namespace-range2 | grep -v -e production | awk '{print $1}')
do
    oc scale dc --all --replicas=1 -n $NS && echo "Succesfully scaled deploymentConfigs to 1 within $NS"
done