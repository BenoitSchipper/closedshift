#!/bin/bash

#Gathers all the namesapces and deletes their respective Horizintal Pod Scalers (HPA).
#Delete on line 7 has been commented to prevent accidental deletions, remove it if you want to run the script.
#This is currently setup to IGNORE production, remove the second grep to include production. Or remove the -v to specifically select production instead.
for NS in $(oc get projects | grep -e example-namespace-range1 -e example-namespace-range2 | grep -v -e production | awk '{print $1}')
do
  #oc delete hpa --all -n $NS
  echo "HPA succesfully deleted for" $NS
done