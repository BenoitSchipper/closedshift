#!/bin/bash

#Folder to place the resulting .yaml scripts
SAVEDIR=./extracted_hpa
SAVEDIRCLEAN=cleaned_hpa

#Creates folders if not present.
mkdir -p "$SAVEDIR"/"$SAVEDIRCLEAN"

#Stores all hpa.yaml in the output directory, per namespace.
#Creates a new file with clean hpa and stores in hpa-clean.yaml, per namespace.
#This is currently setup to IGNORE production, remove the second grep to include production. Or remove the -v to specifically select production instead.
for NS in $(oc get projects | grep -e example-namespace-range1 -e example-namespace-range2 | grep -v -e production | awk '{print $1}')
do
  oc get hpa -n $NS -o yaml > $SAVEDIR/$NS-hpa.yaml
  sed -e "/annotations:/,/creationTimestamp/d" -e "/resourceVersion:/,/uid:/d" -e "/status:/,/lastScaleTime:/d" $SAVEDIR/$NS-hpa.yaml > $SAVEDIR/$SAVEDIRCLEAN/$NS-cleaned-hpa.yaml
  echo $NS"-hpa.yaml stored and cleaned for ->" $NS
done