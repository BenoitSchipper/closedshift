#!/bin/bash

#Folder containing the yaml scripts to be applied.
SAVEDIR=./extracted_hpa/cleaned_hpa

#Iterates through .yaml files within SAVEDIR and apply's the configuration.
for hpa in $(ls $SAVEDIR | grep cleaned ) 
do
    # oc apply -f $SAVEDIR/$hpa
    echo $hpa
done